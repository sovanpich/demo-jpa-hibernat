package org.tutorial;

import org.tutorial.model.Student;
import org.tutorial.repository.StudentRepository;

import java.util.List;

public class App {
    public static void main(String[] args) {
        Student student = new Student();
        student.setFirstname("Sovanpich");
        student.setLastname("Theng");
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();


        StudentRepository studentRepository = new StudentRepository();
        studentRepository.addStudent(student);
        System.out.println("Added Student"+ student.toString());

        student1 = studentRepository.findStudent((int) student.getId());
        System.out.println("Found Student" + student1.toString());

        student=studentRepository.findById((int) student.getId());
        System.out.println("Student (JPQL)"+student.toString());

        student.setLastname("Devid");
        student = studentRepository.updateStudent(student);
        System.out.println("Updated Student"+student.toString());



        List<String> name = studentRepository.findFirstname();
        System.out.println(name);

        studentRepository.findLastname().forEach(System.out::println);
        studentRepository.deleteStudent(student);
        System.out.println("Delete Student" + student.toString());


        studentRepository.close();
    }
}
