package org.tutorial.repository;

import org.tutorial.model.Student;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class StudentRepository {
    private EntityManager entityManager;
    private final EntityManagerFactory entityManagerFactory;

    public StudentRepository(){
        this.entityManagerFactory = Persistence.createEntityManagerFactory("student_pu");
        this.entityManager = entityManagerFactory.createEntityManager();
    }
     public Student addStudent(Student student){
        entityManager.getTransaction().begin();
        entityManager.persist(student);
        entityManager.getTransaction().commit();
        return student;
     }

     public Student findStudent(int id){
         return entityManager.find(Student.class,1);
     }

     public Student findById(int id){
       Query query = entityManager.createNamedQuery("find stu by id");
       query.setParameter("id",id);
        return (Student) query.getSingleResult();

     }

     public Student updateStudent(Student student){
        Student studentToUpdate = findStudent((int) student.getId());
        entityManager.getTransaction().begin();
        studentToUpdate.setFirstname(student.getFirstname());
        studentToUpdate.setLastname(student.getLastname());
        entityManager.getTransaction().commit();
        return studentToUpdate;
     }
    public List<String> findFirstname(){
        Query query = entityManager.createQuery("Select firstname from Student");
        return query.getResultList();
    }
    public List<String> findLastname(){
       Query query = entityManager.createQuery("select lastname FROM Student ");
        return query.getResultList();
    }
     public void deleteStudent(Student student){
        entityManager.getTransaction().begin();
        entityManager.remove(student);
        entityManager.getTransaction().commit();
     }

//     public  List<Student> getStudentWithCriteriaBuilder(){
//         CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//         CriteriaQuery<Student> criteriaQuery = criteriaBuilder.createQuery(Student.class);
//
//         Root<Student> studentRoot = criteriaQuery.from(Student.class);
//         criteriaQuery.select(studentRoot.get("firstname"));
//         CriteriaQuery<Student> select = criteriaQuery.select(studentRoot);
//         TypedQuery<Student> query = entityManager.createQuery(select);
//
//         return query.getResultList();
//     }



    public void close(){
        this.entityManager.close();
        this.entityManagerFactory.close();
    }
}
